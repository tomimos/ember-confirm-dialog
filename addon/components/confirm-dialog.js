import Component from '@ember/component';
import layout from "../templates/components/confirm-dialog";

export default Component.extend({
  layout,
  willDestroyElement() {
    this._super(...arguments);

    let dialogId = this.getWithDefault('confirmDialogId', 'confirmDialog');
    $('#'+dialogId).modal('hide');
  },
  didDestroyElement() {
    this._super(...arguments);

    document.body.className = 'ember-application';
    document.body.style.cssText = '';
  },
  actions: {
    confirm() {
      let dialogId = this.getWithDefault('confirmDialogId', 'confirmDialog');
      console.log(dialogId);
      $('#'+dialogId).modal('hide');
      this.sendAction();
    },
    cancel() {
      let dialogId = this.getWithDefault('confirmDialogId', 'confirmDialog');
      $('#'+dialogId).modal('hide');
    },
  }
});
